package com.codev.gotaxi.data.entities;

import java.io.Serializable;

/**
 * Created by miguel on 31/05/17.
 */

public class UberEntity implements Serializable {
    private String localized_display_name;
    private double distance;
    private String display_name;
    private String currency_code;
    private double high_estimate;
    private double low_estimate;
    private double duration;
    private String estimate;

    public String getLocalized_display_name() {
        return localized_display_name;
    }

    public void setLocalized_display_name(String localized_display_name) {
        this.localized_display_name = localized_display_name;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public double getHigh_estimate() {
        return high_estimate;
    }

    public void setHigh_estimate(double high_estimate) {
        this.high_estimate = high_estimate;
    }

    public double getLow_estimate() {
        return low_estimate;
    }

    public void setLow_estimate(double low_estimate) {
        this.low_estimate = low_estimate;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public String getEstimate() {
        return estimate;
    }

    public void setEstimate(String estimate) {
        this.estimate = estimate;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }
}

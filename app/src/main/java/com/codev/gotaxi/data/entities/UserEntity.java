package com.codev.gotaxi.data.entities;

import java.io.Serializable;

/**
 * Created by junior on 30/09/16.
 */
public class UserEntity implements Serializable {

    private String id;
    private String email;
    private String first_name;
    private String last_name;
    private String cellphone;
    private String birth_date;
    private String picture;
    private String gender;
    private String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone() {
        return cellphone;
    }

    public void setPhone(String phone) {
        this.cellphone = phone;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getPhoto() {
        return picture;
    }

    public void setPhoto(String photo) {
        this.picture = photo;
    }

    public String getGender() {
        if(gender!=null){
            return gender;
        }
        else{
            return "M";
        }
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFullName(){
        String name = "";
        if(first_name!=null && last_name!=null){
            name = first_name+" "+last_name;
        }
        return name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

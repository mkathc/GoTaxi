package com.codev.gotaxi.data.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by miguel on 31/05/17.
 */

public class PricesUberEntity implements Serializable {
    private ArrayList<UberEntity> prices;

    public ArrayList<UberEntity> getPrices() {
        return prices;
    }

    public void setPrices(ArrayList<UberEntity> prices) {
        this.prices = prices;
    }
}

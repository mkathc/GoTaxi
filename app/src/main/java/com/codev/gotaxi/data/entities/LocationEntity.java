package com.codev.gotaxi.data.entities;

import java.io.Serializable;

/**
 * Created by miguel on 19/06/17.
 */

public class LocationEntity implements Serializable {
    private double[] loc;

    public LocationEntity(double[] loc) {
        this.loc = loc;
    }

    public double[] getLoc() {
        return loc;
    }

    public void setLoc(double[] loc) {
        this.loc = loc;
    }
}

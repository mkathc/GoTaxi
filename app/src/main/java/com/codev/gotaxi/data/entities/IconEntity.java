package com.codev.gotaxi.data.entities;

import java.io.Serializable;

/**
 * Created by miguel on 31/05/17.
 */

public class IconEntity implements Serializable {
    private String regular;

    public String getRegular() {
        return regular;
    }

    public void setRegular(String regular) {
        this.regular = regular;
    }
}

package com.codev.gotaxi.data.entities;

import java.io.Serializable;

/**
 * Created by miguel on 31/05/17.
 */

public class CabifyEntity implements Serializable {
    private VehicleTypeEntity vehicle_type;
    private double total_price;
    private String price_formatted;
    private String currency;
    private String currency_symbol;

    public VehicleTypeEntity getVehicle_type() {
        return vehicle_type;
    }

    public void setVehicle_type(VehicleTypeEntity vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    public String getFormatted_price() {
        return price_formatted;
    }

    public void setFormatted_price(String formatted_price) {
        this.price_formatted = formatted_price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency_symbol() {
        return currency_symbol;
    }

    public void setCurrency_symbol(String currency_symbol) {
        this.currency_symbol = currency_symbol;
    }

}

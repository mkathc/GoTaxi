package com.codev.gotaxi.presentation.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.RelativeLayout;

import com.codev.gotaxi.R;
import com.codev.gotaxi.core.BaseActivity;
import com.codev.gotaxi.presentation.auth.LoginFragment;
import com.codev.gotaxi.presentation.auth.LoginPresenter;
import com.codev.gotaxi.presentation.utils.ActivityUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by miguel on 24/05/17.
 */

public class LoginActivity extends BaseActivity {

    @BindView(R.id.body)
    RelativeLayout body;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clear);
        ButterKnife.bind(this);

        LoginFragment fragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.body);
        if(fragment==null){
            fragment = LoginFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),fragment,R.id.body);
        }

        new LoginPresenter(fragment,getApplicationContext());
    }
}

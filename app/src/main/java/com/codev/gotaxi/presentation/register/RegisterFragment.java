package com.codev.gotaxi.presentation.register;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.codev.gotaxi.R;
import com.codev.gotaxi.core.BaseActivity;
import com.codev.gotaxi.core.BaseFragment;
import com.codev.gotaxi.data.entities.UserEntity;
import com.codev.gotaxi.presentation.load.LoadActivity;
import com.codev.gotaxi.presentation.utils.ProgressDialogCustom;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by miguel on 14/06/17.
 */

public class RegisterFragment extends BaseFragment implements Validator.ValidationListener, RegisterContract.View {

    @NotEmpty(message = "Este campo no puede ser vacío")
    @BindView(R.id.et_name)
    EditText etName;
    @NotEmpty(message = "Este campo no puede ser vacío")
    @BindView(R.id.et_last_name)
    EditText etLastName;
    @BindView(R.id.et_birth_date)
    EditText etBirthDate;
    @Email(message = "No es un email válido")
    @BindView(R.id.et_email)
    EditText etEmail;
    @Password(scheme = Password.Scheme.ANY, message = "Contraseña no posee 6 dígitos")
    @BindView(R.id.et_password)
    EditText etPassword;
    @ConfirmPassword(message = "No concuerda con contraseña")
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;
    @BindView(R.id.rb_male)
    RadioButton rbMale;
    @BindView(R.id.rb_female)
    RadioButton rbFemale;
    Unbinder unbinder;
    @BindView(R.id.rg_gender)
    RadioGroup rgGender;
    @NotEmpty(message = "Este campo no puede ser vacío")
    @BindView(R.id.et_cellphone)
    EditText etCellphone;

    private RegisterContract.Presenter presenter;
    private Validator validator;
    private ProgressDialogCustom dialogCustom;

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_register, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        validator = new Validator(this);
        validator.setValidationListener(this);
        dialogCustom = new ProgressDialogCustom(getContext(), "Registrando...");
        etBirthDate.setFocusable(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.et_birth_date)
    public void onViewClicked() {
    }

    public void register() {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        UserEntity userEntity = new UserEntity();
        userEntity.setFirst_name(etName.getText().toString());
        userEntity.setLast_name(etLastName.getText().toString());
        userEntity.setEmail(etEmail.getText().toString());
        userEntity.setPassword(etPassword.getText().toString());
        userEntity.setPhone(etCellphone.getText().toString());
        userEntity.setGender(rgGender.getCheckedRadioButtonId()==rbMale.getId()?"M":"F");
        presenter.register(userEntity);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else if (view instanceof TextView) {
                ((TextView) view).setError(message);
            } else {
                showErrorMessage(message);
            }
        }
    }

    @Override
    public void registerSuccessfully() {
        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setTitle("Registro exitoso")
                .setMessage("Se registro su usuario con éxito")
                .setPositiveButton("ACEPTAR",null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        nextActivity(getActivity(),null, LoadActivity.class,true);
                    }
                })
                .create();
        alertDialog.show();
    }

    @Override
    public void setPresenter(RegisterContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (dialogCustom != null) {
            if (active) {
                dialogCustom.show();
            } else {
                dialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }
}

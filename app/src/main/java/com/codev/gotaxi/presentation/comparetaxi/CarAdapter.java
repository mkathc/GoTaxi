package com.codev.gotaxi.presentation.comparetaxi;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codev.gotaxi.R;
import com.codev.gotaxi.data.entities.CarEntity;
import com.codev.gotaxi.presentation.utils.GlideUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by miguel on 19/06/17.
 */

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {



    private ArrayList<CarEntity> list;
    private Context context;

    public CarAdapter(ArrayList<CarEntity> list) {
        this.list = list;
    }

    public void setItems(ArrayList<CarEntity> list, Context context) {
        this.list = list;
        this.context = context;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_taxi, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CarEntity carEntity = list.get(position);
        holder.tvTaxiType.setText(carEntity.getServiceName());
        holder.tvTaxiPrice.setText(carEntity.getEstimate());

        if (carEntity.getImagerCar() != null) {
            if (!carEntity.getImagerCar().equals("SIN FOTO")) {
                GlideUtils.loadImage(holder.imageCar, carEntity.getImagerCar(), context);
            }
        } else {
            holder.imageCar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_uber));

        }

        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickCard(carEntity.getTypeCar());

            }
        });
    }


    private void onClickCard(int tipe) {
        switch (tipe) {
            case 1:
                PackageManager pm = context.getPackageManager();
                try {
                    pm.getPackageInfo("com.ubercab", PackageManager.GET_ACTIVITIES);
                    String uri = "uber://?action=setPickup";
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(uri));
                    context.startActivity(intent);
                } catch (PackageManager.NameNotFoundException e) {
                    try {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.ubercab")));
                    } catch (ActivityNotFoundException anfe) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.ubercab")));
                    }
                }
                break;
            case 2:
                PackageManager pm2 = context.getPackageManager();
                try {
                    pm2.getPackageInfo("com.cabify.rider", PackageManager.GET_ACTIVITIES);
                    String uri = "cabify://cabify";
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(uri));
                    context.startActivity(intent);
                } catch (PackageManager.NameNotFoundException e) {
                    try {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cabify.rider")));
                    } catch (ActivityNotFoundException anfe) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.cabify.rider")));
                    }
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_taxi_type)
        TextView tvTaxiType;
        @BindView(R.id.tv_taxi_price)
        TextView tvTaxiPrice;
        @BindView(R.id.image_car)
        ImageView imageCar;
        @BindView(R.id.cardview)
        CardView cardview;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

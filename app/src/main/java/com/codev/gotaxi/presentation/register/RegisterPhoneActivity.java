package com.codev.gotaxi.presentation.register;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.codev.gotaxi.R;
import com.codev.gotaxi.core.BaseActivity;
import com.codev.gotaxi.presentation.register.RegisterPhoneFragment;
import com.codev.gotaxi.presentation.register.RegisterPhonePresenter;
import com.codev.gotaxi.presentation.utils.ActivityUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by miguel on 24/05/17.
 */

public class RegisterPhoneActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private RegisterPhoneFragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setTitle("Completar Registro");

        fragment = (RegisterPhoneFragment) getSupportFragmentManager().findFragmentById(R.id.body);
        if (fragment == null) {
            fragment = RegisterPhoneFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), fragment, R.id.body);
        }
        new RegisterPhonePresenter(fragment, getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.check, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_check:
                if (fragment != null) {
                    fragment.registerPhone();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

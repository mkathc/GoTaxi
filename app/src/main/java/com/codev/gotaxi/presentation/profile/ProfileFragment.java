package com.codev.gotaxi.presentation.profile;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codev.gotaxi.R;
import com.codev.gotaxi.core.BaseActivity;
import com.codev.gotaxi.core.BaseFragment;
import com.codev.gotaxi.data.entities.UserEntity;
import com.codev.gotaxi.presentation.utils.CircleTransform;
import com.codev.gotaxi.presentation.utils.ProgressDialogCustom;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by miguel on 27/06/17.
 */

public class ProfileFragment extends BaseFragment implements ProfileContract.View, Validator.ValidationListener {


    @BindView(R.id.iv_profile_picture)
    ImageView ivProfilePicture;
    @NotEmpty(message = "Este campo no puede ser vacío")
    @BindView(R.id.etFirstName)
    EditText etFirstName;
    @NotEmpty(message = "Este campo no puede ser vacío")
    @BindView(R.id.etLastName)
    EditText etLastName;
    @Email(message = "Este campo no posee formato de correo electrónico")
    @BindView(R.id.etEmail)
    EditText etEmail;
    @Length(max = 9,min = 9,message = "Este campo no posee 9 dígitos")
    @BindView(R.id.etCellphone)
    EditText etCellphone;
    @BindView(R.id.rb_male)
    RadioButton rbMale;
    @BindView(R.id.rb_female)
    RadioButton rbFemale;
    @BindView(R.id.rg_gender)
    RadioGroup rgGender;
    Unbinder unbinder;
    @BindView(R.id.btn_save)
    Button btnSave;

    private boolean edit = false;
    private ProfileContract.Presenter presenter;
    private ProgressDialogCustom dialogCustom;
    private UserEntity userEntity;
    private Validator validator;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_account, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        validator = new Validator(this);
        validator.setValidationListener(this);
        dialogCustom = new ProgressDialogCustom(getContext(), "Editando...");
        setupEditable(edit);
        etCellphone.setEnabled(false);
        etEmail.setEnabled(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    private void setupEditable(boolean active) {
        etFirstName.setEnabled(active);
        etLastName.setEnabled(active);
        rbMale.setEnabled(active);
        rbFemale.setEnabled(active);
        btnSave.setVisibility(active?View.VISIBLE:View.GONE);
    }

    @Override
    public void loadProfile(UserEntity userEntity) {
        this.userEntity = userEntity;
        etFirstName.setText(userEntity.getFirst_name());
        etLastName.setText(userEntity.getLast_name());
        etCellphone.setText(userEntity.getPhone());
        etEmail.setText(userEntity.getEmail());
        if(userEntity.getGender().equals("M")){
            rbMale.setChecked(true);
        }
        else{
            rbFemale.setChecked(true);
        }
        if(userEntity.getPhoto()!=null){
            Glide.with(this).load(userEntity.getPhoto()).bitmapTransform(new CircleTransform(getContext())).into(ivProfilePicture);
        }
    }

    @Override
    public void editProfile() {
        edit = !edit;
        setupEditable(edit);
        if(!edit && userEntity!=null){
            loadProfile(userEntity);
        }
    }

    @Override
    public void returnProfile() {
        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setTitle("Usuario Editado")
                .setPositiveButton("Aceptar",null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        ((ProfileActivity)getActivity()).updateProfile();
                    }
                })
                .create();
        alertDialog.show();
    }

    @Override
    public void setPresenter(ProfileContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (dialogCustom != null) {
            if (active) {
                dialogCustom.show();
            } else {
                dialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onValidationSucceeded() {
        userEntity.setFirst_name(etFirstName.getText().toString());
        userEntity.setLast_name(etLastName.getText().toString());
        userEntity.setEmail(etEmail.getText().toString());
        userEntity.setPhone(etCellphone.getText().toString());
        userEntity.setGender(rgGender.getCheckedRadioButtonId()==rbMale.getId()?"M":"F");
        presenter.editProfile(userEntity);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else if (view instanceof TextView) {
                ((TextView) view).setError(message);
            } else {
                showErrorMessage(message);
            }
        }
    }

    @OnClick(R.id.btn_save)
    public void onViewClicked() {
        validator.validate();
    }
}
